﻿using DataAccess.Entities;
using DataAccess.Validation;
using NUnit.Framework;

namespace DataAccess.Tests
{
    [TestFixture]
    public class UserRegistrationTests
    {
        [Test]
        public void TestUserHelper_ValidUser()
        {
            var helper = new UserValidationHelper(null);

            var user = new UserDto
            {
                Firstname = "Test",
                Lastname = "Test",
                Email = "Test@test.com",
                Password = "Password1",
                ConfirmPassword = "Password1"

            };

            var expectedErrors = 0;

            Assert.AreEqual(expectedErrors, helper.IsUserValid(user).Count);
        }

        [Test]
        public void TestUserHelper_InvalidUser()
        {
            var helper = new UserValidationHelper(null);
            var expectedErrors = 5;

            var user = new UserDto
            {
                Firstname = "",
                Lastname = "",
                Email = "",
                Password = "",
                ConfirmPassword = "X"
            };

            var actualErrors = helper.IsUserValid(user).Count;
            Assert.AreEqual(expectedErrors, actualErrors);

            user.Firstname = "Bob";
            expectedErrors = 4;
            actualErrors = helper.IsUserValid(user).Count;

            Assert.AreEqual(expectedErrors, actualErrors);

            user.Lastname = "Smith";
            expectedErrors = 3;
            actualErrors = helper.IsUserValid(user).Count;

            Assert.AreEqual(expectedErrors, actualErrors);

            user.Email = "Bob@email.com";
            expectedErrors = 2;
            actualErrors = helper.IsUserValid(user).Count;

            Assert.AreEqual(expectedErrors, actualErrors);

            user.Password = "Password1";
            expectedErrors = 1;
            actualErrors = helper.IsUserValid(user).Count;

            Assert.AreEqual(expectedErrors, actualErrors);

            user.ConfirmPassword = "Password1";
            expectedErrors = 0;
            actualErrors = helper.IsUserValid(user).Count;

            Assert.AreEqual(expectedErrors, actualErrors);
        }

        [Test]
        public void PasswordEncryptionIsWorking()
        {
            var password = "Password1";
            var encryptedPassword = PasswordHelper.EncryptPassword(password);

            Assert.IsTrue(PasswordHelper.VerifyPassword(password, encryptedPassword));
        }
    }
}
