﻿CREATE PROCEDURE InsertUser
    @firstName NVARCHAR(75) ,
    @lastName NVARCHAR(75) ,
    @email NVARCHAR(250) ,
    @password NVARCHAR(250)
AS
    BEGIN
        INSERT  INTO [dbo].[Users]
                ( [FirstName], [LastName], [EmailAddress], [Password] )
        VALUES  ( @firstName, @lastName, @email, @password );
    END;
GO
