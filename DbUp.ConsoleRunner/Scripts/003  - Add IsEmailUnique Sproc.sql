﻿CREATE PROCEDURE IsEmailUnique
    @email NVARCHAR(250)
AS
    BEGIN
        SELECT COUNT(1)
		FROM dbo.Users 
		WHERE EmailAddress = @email

    END;
GO
