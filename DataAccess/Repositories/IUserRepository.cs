﻿using System.Collections.Generic;
using DataAccess.Entities;

namespace DataAccess.Repositories
{
    /// <summary>
    /// Contract for User creation methods
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Used to save a new user to the database
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        bool CreateNewUser(UserDto user);

        /// <summary>
        /// Validates the supplied user details
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        IList<string> GetUserValidatationErrors(UserDto user);
    }
}
