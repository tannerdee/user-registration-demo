﻿using System.Collections.Generic;
using DataAccess.Entities;
using DataAccess.Validation;

namespace DataAccess.Repositories
{
    /// <summary>
    /// User Repository to manage User related data access 
    /// </summary>
    public class UserRepository : IUserRepository
    {
        private static readonly DataContext _dataContext = new DataContext();
        private readonly UserValidationHelper _helper = new UserValidationHelper(_dataContext);

        public UserRepository()
        {
            
        }

        /// <summary>
        /// Creates the specified user and returns true if the operation was successful
        /// and false otherwise
        /// </summary>
        /// <param name="user"></param>
        public bool CreateNewUser(UserDto user)
        {
            return _dataContext.CreateNewUser(user);
        }

        /// <summary>
        /// Validates the supplied user details returns a list of errors if any
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public IList<string> GetUserValidatationErrors(UserDto user)
        {
            return _helper.IsUserValid(user);
        }
    }
}