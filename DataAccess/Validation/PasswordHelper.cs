﻿namespace DataAccess.Validation
{
    /// <summary>
    /// Password encryption / verification helper
    /// </summary>
    public static class PasswordHelper
    {
        /// <summary>
        /// Encrypt user password
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string EncryptPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }

        /// <summary>
        /// Verify password match
        /// </summary>
        /// <param name="password"></param>
        /// <param name="encryptedPassword"></param>
        /// <returns></returns>
        public static bool VerifyPassword(string password, string encryptedPassword)
        {
            return BCrypt.Net.BCrypt.Verify(password, encryptedPassword);
        }
    }
}
