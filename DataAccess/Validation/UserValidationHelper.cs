﻿using System.Collections.Generic;
using DataAccess.Entities;

namespace DataAccess.Validation
{
    public class UserValidationHelper
    {
        private readonly DataContext _dataContext;

        public UserValidationHelper(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        /// <summary>
        /// Server validation to ensure user is valid
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public IList<string> IsUserValid(UserDto user)
        {
            var errors = new List<string>();

            if(user.Firstname == string.Empty)
                errors.Add("First name is empty.");

            if (user.Lastname == string.Empty)
                errors.Add("Last name is empty.");

            if (user.Email == string.Empty)
                errors.Add("Email is empty.");

            if(user.Password == string.Empty)
                errors.Add("Password is empty.");

            if (user.Password != user.ConfirmPassword)
                errors.Add("Passwords must match.");

            // Hack to enable testing without data connection without mocking
            if (!IsEmailUnique(user.Email))
                errors.Add("Email is not unique.");

            return errors;
        }

        /// <summary>
        /// Checks in database if email is already present
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        private bool IsEmailUnique(string email)
        {
            if (_dataContext == null)
                return true;

            return _dataContext.IsEmailUnique(email);
        }
    }
}
