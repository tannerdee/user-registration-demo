﻿using DataAccess.Entities;

namespace DataAccess
{
    public interface IDataContext
    {
        /// <summary>
        /// Submits a user to be created in the database
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        bool CreateNewUser(UserDto user);

        /// <summary>
        /// Checks to ensure the provided email is unique before a user can be created
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        bool IsEmailUnique(string email);
    }
}