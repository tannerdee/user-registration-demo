﻿using System;
using System.Data;
using System.Data.SqlClient;
using DataAccess.Entities;
using DataAccess.Properties;
using DataAccess.Validation;

namespace DataAccess
{
    public class DataContext : IDataContext
    {
        private SqlConnection _connection;
        private SqlCommand _command;

        /// <summary>
        /// Submits a user to be created in the database
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool CreateNewUser(UserDto user)
        {
            try
            {
                var commandText = "InsertUser";
                _connection = GetSqlConnection();

                using (_connection)
                using (_command = new SqlCommand(commandText, _connection) { CommandType = CommandType.StoredProcedure })
                {
                    _command.Parameters.Add("@FirstName", SqlDbType.NVarChar, 75).Value = user.Firstname;
                    _command.Parameters.Add("@Lastname", SqlDbType.NVarChar, 75).Value = user.Lastname;
                    _command.Parameters.Add("@Email", SqlDbType.NVarChar, 250).Value = user.Email;
                    _command.Parameters.Add("@Password", SqlDbType.NVarChar, 250).Value = PasswordHelper.EncryptPassword(user.Password);

                    _connection.Open();
                    _command.ExecuteNonQuery();
                }

                // Log user creation somewhere
                return true;
            }
            catch (Exception e)
            {
                // Log e.Message somewhere
                Console.WriteLine(e.Message);
                return false;
            }
        }

        /// <summary>
        /// Checks to ensure the provided email is unique before a user can be created
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool IsEmailUnique(string email)
        {
            try
            {
                int result;
                var commandText = "IsEmailUnique";
                _connection = GetSqlConnection();

                using (_connection)
                using (_command = new SqlCommand(commandText, _connection) { CommandType = CommandType.StoredProcedure })
                {
                    _command.Parameters.Add("@Email", SqlDbType.NVarChar, 250).Value = email;

                    _connection.Open();
                    result = (int)_command.ExecuteScalar();
                    _command.ExecuteNonQuery();
                }

                return result == 0;
            }
            catch (Exception)
            {
                // Log e.Message somewhere
                return false;
            }
        }

        private SqlConnection GetSqlConnection()
        {
            return new SqlConnection(Settings.Default.ConnectionString);
        }
    }
}
