﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class User
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Required")]
        [RegularExpression(@"^[^\<\>]*$", ErrorMessage = "Cannot contain: < or >")]
        [MaxLength(75, ErrorMessage = "Max length 75")]
        public string Firstname { get; set; }

        [Required(ErrorMessage = "Required")]
        [RegularExpression(@"^[^\<\>]*$", ErrorMessage = "Cannot contain: < or >")]        
        [MaxLength(75, ErrorMessage = "Max length 75")]
        public string Lastname { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.EmailAddress)]
        [MaxLength(250, ErrorMessage = "Max length 250")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [MaxLength(250, ErrorMessage = "Max length 250")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{6,}", 
            ErrorMessage = "<span>Password complexity requires:</span>" +
                           "<ul>" +
                           "<li>Uppercase</li>" +
                           "<li>Lowercase</li>" +
                           "<li>Number</li>" +
                           "<li>Min length 6 characters</li>" +
                           "</ul>")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Matching password required")]
        public string ConfirmPassword { get; set; }
    }
}