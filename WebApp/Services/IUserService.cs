﻿using System.Collections.Generic;
using WebApp.Models;

namespace WebApp.Services
{
    public interface IUserService
    {
        string CreateNewUser(User user);

        IList<string> GetUserValidatationErrors(User user);
    }
}