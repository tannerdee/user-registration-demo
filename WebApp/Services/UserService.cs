﻿using System;
using System.Collections.Generic;
using DataAccess.Entities;
using DataAccess.Repositories;
using WebApp.Models;

namespace WebApp.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        /// <summary>
        /// Create user by calling the user repository
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public string CreateNewUser(User user)
        {
            var response = string.Empty;

            try
            {
                var userDto = UserToDto(user);

                if (_userRepository.CreateNewUser(userDto))
                    response = $"User: {user.Firstname} {user.Lastname} was created.";
            }
            catch (Exception e)
            {
                response = $"User creation error: {e.Message}";
            }

            return response;
        }

        /// <summary>
        /// Requests any errors from the user repository
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public IList<string> GetUserValidatationErrors(User user)
        {
            var userDto = UserToDto(user);

            return _userRepository.GetUserValidatationErrors(userDto);
        }

        /// <summary>
        /// Maps a user to a dto
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private static UserDto UserToDto(User user)
        {
            return new UserDto
            {
                Firstname = user.Firstname,
                Lastname = user.Lastname,
                Email = user.Email,
                Password = user.Password,
                ConfirmPassword = user.ConfirmPassword
            };
        }
    }
}