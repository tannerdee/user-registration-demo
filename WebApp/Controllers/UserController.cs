﻿using System;
using System.Linq;
using System.Web.Mvc;
using WebApp.Models;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        public ActionResult CreateUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateUser(User user)
        {
            var validationErrors = _userService.GetUserValidatationErrors(user);
            if (validationErrors.Count > 0)
            {
                var errorString = String.Join(", ", validationErrors.ToArray());
                return Content($"Error saving user: {errorString}");
            }

            return Content(_userService.CreateNewUser(user));
        }


    }
}