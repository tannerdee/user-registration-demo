## Introduction

This project is User Registration Demo web page, that allows saving a user to a SQL Server Database.

It was built in VS2017 using C# and saving to a SQL Server 2014.

---

## Setup

The solution uses [DbUp](https://dbup.readthedocs.io/en/latest/) to manage the database setup. In order to run the solution, you should:

1. Open the solution.
2. Locate the **DbUp.ConsoleRunner** project.
3. Right click and select **Debug > Start new instance**.

This will launch a console application to generate the database and execute the required scripts to add the database objects.

---
